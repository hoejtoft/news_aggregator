﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="Scripts/jquery-3.0.0.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="CSS/style.css" rel="stylesheet" />
    <script src="Scripts/Custom/custom.js"></script>
    <title></title>
</head>
<body>
    <div id="loader"></div>
    <form id="form1" runat="server">
        <div id="filtermenu">
            <div id="selectArticle">
                <label for="selectarticles">Number of articles:</label>
                <select name="selectarticles" class="custom-select mr-sm-2" id="AmountArticles">
                    <option value="0" selected="">0</option>
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>

            <div class="container">
                <ul class="Categories">
                    <label for="_sitex">Collective evolution</label>
                    <div class="dropdown">
                        <li>
                            <label for="_Collective1">All news</label>
                            <input class="_check" id="_Collective1" name="sites" type="checkbox" value="http://www.collective-evolution.com/" />
                        </li>
                        <li>
                            <label for="_Collective2">Consciousness</label>
                            <input class="_check" id="_Collective2" name="sites" type="checkbox" value="http://www.collective-evolution.com/category/consciousness/" />
                        </li>
                        <li>
                            <label for="_Collective3">News</label>
                            <input class="_check" id="_Collective3" name="sites" type="checkbox" value="http://www.collective-evolution.com/category/alternative-news/" />
                        </li>
                        <li>
                            <label for="_Collective4">Health</label>
                            <input class="_check" id="_Collective4" name="sites" type="checkbox" value="http://www.collective-evolution.com/category/health/" />
                        </li>
                        <li>
                            <label for="_Collective5">Science</label>
                            <input class="_check" id="_Collective5" name="sites" type="checkbox" value="http://www.collective-evolution.com/category/sci-tech/" />
                        </li>
                    </div>
                </ul>
                <ul class="Categories">
                    <label for="_sitex">The free thought project</label>
                    <div class="dropdown">
                        <li>
                            <label for="_Free1">All news</label>
                            <input class="_check" id="_Free1" name="sites" type="checkbox" value="http://thefreethoughtproject.com/" />
                        </li>
                        <li>
                            <label for="_Free2">Solutions</label>
                            <input class="_check" id="_Free2" name="sites" type="checkbox" value="https://thefreethoughtproject.com/category/solutions/" />
                        </li>
                        <li>
                            <label for="_Free3">Police Accountability</label>
                            <input class="_check" id="_Free3" name="sites" type="checkbox" value="https://thefreethoughtproject.com/category/cop-watch/" />
                        </li>
                        <li>
                            <label for="_Free4">Government corruption</label>
                            <input class="_check" id="_Free4" name="sites" type="checkbox" value="https://thefreethoughtproject.com/category/be-the-change/government-corruption/" />
                        </li>
                        <li>
                            <label for="_Free5">War on drugs</label>
                            <input class="_check" id="_Free5" name="sites" type="checkbox" value="https://thefreethoughtproject.com/category/be-the-change/war-on-drugs-be-the-change/" />
                        </li>
                        <li>
                            <label for="_Free6">Foreign affairs</label>
                            <input class="_check" id="_Free6" name="sites" type="checkbox" value="https://thefreethoughtproject.com/category/foreign-affairs/" />
                        </li>
                    </div>
                </ul>
                <ul class="Categories">
                    <label for="_sitex">Greenmedinfo</label>
                    <div class="dropdown">
                        <li for="_Green1">
                            <label for="_Green1">All news</label>
                            <input class="_check" id="_Green1" name="sites" type="checkbox" value="http://www.greenmedinfo.com/gmi-blogs" />
                        </li>
                    </div>
                </ul>

            </div>
        </div>
        <div id="ShowFrame" runat="server">
            <div class="container">
                <div class="row">
                    <div id="news_article" runat="server"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
