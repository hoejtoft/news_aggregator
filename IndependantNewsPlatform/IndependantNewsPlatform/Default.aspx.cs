﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using HtmlAgilityPack;
using Supremes;
using Supremes.Nodes;

public partial class _Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod]
    public static string SendValues(List<string> param,int articles)
    {
        var result = "";
        if (param.Count > 0)
        {
            result = addArticle(param, param.Count,articles);
        }
        return result;
    }
    public static string addArticle(List<string> url, int lenght,int amount)
    {
        Elements _article = null;
        string html = "";
        for (int i = 0; i < lenght; i++)
        {
            var count = 0;
            var doc = Dcsoup.Parse(new Uri(url[i]), 10000);
            _article = doc.Select(MapUrlToClass(url[i]));
            foreach(var arr in _article)
            {
                if(count >= amount)
                {
                    return html;
                }
                if (count % 3 == 0)
                {
                    html += "<div class='row'>";

                }
                html += "<div class='col-md-4'>";
                html += arr;
                html += "</div>";
                count++;
                if (count % 3 == 0)
                {
                    html += "</div>";

                }
            }

        }
        return html;
    }
    public static string MapUrlToClass(string url)
    {
        switch (url)
        {
            case "http://www.collective-evolution.com/":
                return ".cb-article";
            case "http://www.collective-evolution.com/category/consciousness/":
                return ".cb-article";
            case "http://www.collective-evolution.com/category/alternative-news/":
                return ".cb-article";
            case "http://www.collective-evolution.com/category/health/":
                return ".cb-article";
            case "http://www.collective-evolution.com/category/sci-tech/":
                return ".cb-article";
            case "http://thefreethoughtproject.com/":
                return ".td-block-span6";
            case "https://thefreethoughtproject.com/category/solutions/":
                return ".td-block-span6";
            case "https://thefreethoughtproject.com/category/cop-watch/":
                return ".td-block-span6";
            case "https://thefreethoughtproject.com/category/be-the-change/government-corruption/":
                return ".td-block-span6";
            case "https://thefreethoughtproject.com/category/be-the-change/war-on-drugs-be-the-change/":
                return ".td-block-span6";
            case "https://thefreethoughtproject.com/category/foreign-affairs/":
                return ".td-block-span6";
            case "http://www.greenmedinfo.com/gmi-blogs":
                return ".views-row";
            default:
                return "";
        }
    }
    
}