﻿$(document).ready(function () {
    $("#loader").css("display", "none");
    var checked = [];
    SetCheckboxes();
    setAmountOfArticles();
});

function SetCheckboxes() {
    $('.Categories li').click(function () {
        var $cb = $(this).find(":checkbox");
        if (!$cb.prop("checked")) {
            $cb.prop("checked", true);
        } else {
            $cb.prop("checked", false);
        }
        GetCheckboxValues();
        FilterWebsites();
    });
}
function setAmountOfArticles() {
    $("#AmountArticles").on("change", function () {
        GetCheckboxValues();
        FilterWebsites();
    });
}
function FilterWebsites() {
    var amountofarticles = $("#AmountArticles").val();
    $("#loader").css("display", "block");
    $.ajax({
        type: "POST",
        url: "Default.aspx/SendValues",
        data: JSON.stringify({ "param": checked, "articles": amountofarticles}),
        contentType: "application/json",
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#loader").css("display", "none");
            alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
        },
        success: function (result) {
            $("#loader").css("display", "none");
            var data = result.d;
            addNews(result.d);
            console.log("We returned: " + result.d);
        }

    });
}
function addNews(html) {
    $("#news_article").html("");
    $("#news_article").append(html);
    ClearEmptyArticles();
    addTagetBlank();
    fixImage();
    addBrokenLinks();
}

function fixImage() {
    var img = $('.entry-thumb');
    var test = "";
    img.each(function () {
        this.src = this.srcset;
        test = this.src;
        var part = test.split(".jpg")
        $(this).attr("src", part[0] + ".jpg");
        $(this).attr("srcset", "");
        $(this).attr("sizes", "");
        $(this).css("display", "block");
        $(this).css("visibility", "visible");
    });

}
function addBrokenLinks() {
    var attr = $(".field-content a");
    attr.each(function () {
        var href = this.href;
        var link = href.split("/blog");
        var url = "http://www.greenmedinfo.com/blog"
        $(this).attr("href", url+link[1]);

        console.log(link[1]);
    });
}
function addTagetBlank() {
    setTimeout(function () {
        $("a").attr("target", "_blank");
    }, 1000);
}
function GetCheckboxValues() {
    checked = [];
    $("input[name='sites']:checked").each(function () {
        checked.push($(this).val());
    });
    console.log(checked.length);
}
function ClearEmptyArticles() {
    $(".updated:contains('January 1, 1970')").parent().parent().parent().prev().css("display", "none");
    $(".updated:contains('January 1, 1970')").parent().parent().parent().css("display", "none");
}